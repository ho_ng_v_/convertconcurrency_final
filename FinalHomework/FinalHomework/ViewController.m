//
//  ViewController.m
//  FinalHomework
//
//  Created by Vũ hoàng on 7/5/18.
//  Copyright © 2018 Vũ hoàng. All rights reserved.
//

#import "ViewController.h"
#import "Localisator.h"
#import <AFNetworking/AFNetworking.h>
#import "CountryDTO.h"
#import <SVProgressHUD/SVProgressHUD.h>
typedef void(^onSuccessReturn)(NSArray* object);
typedef void(^onFailedReturn)(NSError* errorObject);
@interface ViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>


@end

NSString *API_LIST_COUNTRIES = @"https://restcountries.eu/rest/v2/all";
NSString *API_GET_RATE_CONCURENCY = @"http://free.currencyconverterapi.com/api/v5/convert?q=%@_%@&compact=y";

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Localize hello text with language of System
    self.lblHello.text = LOCALIZATION(@"titleHello");
    self.txtInputMoney.delegate = self;
    
    //Get List Countries
    [SVProgressHUD show];
    [self GetListCountriesOnSuccess:^(NSArray *object) {
        [self.pickerFirstCountry reloadAllComponents];
        [self.pickerSecondCountry reloadAllComponents];
         self.firstCountry =  [self.listCountry objectAtIndex:1];
         self.secondCountry =  [self.listCountry objectAtIndex:1];
    } onFailure:^(NSError *errorObject) {
        [SVProgressHUD dismiss];
        NSLog(@"Failed to get list Countries");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Failed to get list Countries." preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];
   
  
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Method get list Countries
- (void) GetListCountriesOnSuccess:(onSuccessReturn)success onFailure:(onFailedReturn)failure
{
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:API_LIST_COUNTRIES parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSArray *arrData = [self parseCountries: responseObject];
            self.listCountry = [arrData copy];
             success(nil);
             [SVProgressHUD dismiss];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Failed list Countries");
              failure(error);
             [SVProgressHUD dismiss];
        }];
    
    
}

//Method get Rate concurrency
- (void) GetRateBetweenFirstCurrentcy: (NSString*) firstCurrency withSecondCurrency: (NSString*) secondCurrency OnSuccess: (onSuccessReturn)success onFailure:(onFailedReturn)failure
{
    NSString *composedEndPoint = [NSString stringWithFormat:API_GET_RATE_CONCURENCY, firstCurrency, secondCurrency];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:composedEndPoint parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSString *keyToGetValue = [NSString stringWithFormat:@"%@_%@",firstCurrency,secondCurrency];
        NSNumber *rateConcurrency =[NSNumber numberWithDouble: [[[responseObject objectForKey:keyToGetValue] objectForKey:@"val"] doubleValue]];
        NSArray *arrData = @[rateConcurrency];
        success(arrData);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Failed to get rate of concurrency");
        failure(error);
    }];
    
    
}


- (NSArray*)parseCountries:(NSArray*)data
{
    NSMutableArray* listCountry = [NSMutableArray new];
    for (NSDictionary* dic in data) {
        CountryDTO* country = [CountryDTO new];
        country.countryName = [dic objectForKey:@"name"];
        country.countryCurrency = [[[dic objectForKey:@"currencies"] objectAtIndex:0] objectForKey:@"code"] ;
        country.countryCurrencySymbol = [[[dic objectForKey:@"currencies"] objectAtIndex:0] objectForKey:@"symbol"] ;
         country.languageId = [[[dic objectForKey:@"languages"] objectAtIndex:0] objectForKey:@"iso639_1"] ;
        [listCountry addObject:country];
    }
    return listCountry;
}



#pragma PickerDelegate
-(long) numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(long) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component   {
    return [self.listCountry count];

}

-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component{
    CountryDTO *item =  [self.listCountry objectAtIndex:row];
    return item.countryName;

}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:   (NSInteger)component{
    if(![self checkIsDoubleValue:_txtInputMoney.text])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please input valid number." preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            [self.txtInputMoney becomeFirstResponder];
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    if(pickerView == self.pickerFirstCountry)
    {
          self.firstCountry =  [self.listCountry objectAtIndex:row];
        [[Localisator sharedInstance] setLanguage:self.firstCountry.languageId];
        self.lblHello.text = LOCALIZATION(@"titleHello");
    }
    
    if(pickerView == self.pickerSecondCountry)
    {
       self.secondCountry =   [self.listCountry objectAtIndex:row];
    }
    
    [self GetRateBetweenFirstCurrentcy:self.firstCountry.countryCurrency withSecondCurrency:self.secondCountry.countryCurrency OnSuccess:^(NSArray *object) {
        double rateConcurrency = [[object objectAtIndex:0] doubleValue];
        double convertedValue = [self.txtInputMoney.text doubleValue]*rateConcurrency;
        self.txtConvertedMoney.text = [NSString stringWithFormat:@"%f", convertedValue];
    } onFailure:^(NSError *errorObject) {
        NSLog(@"Failed to get rate of concurrency");
        NSLog(@"Failed to get list Countries");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Failed to convert concurrency" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];
    
}

#pragma TextView delegate - Validate input data
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == self.txtInputMoney)
    {
       if(![self checkIsDoubleValue:textField.text])
       {
           UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please input valid number." preferredStyle:UIAlertControllerStyleAlert];
           
           [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
               
               [textField becomeFirstResponder];
           }]];
           
           [self presentViewController:alertController animated:YES completion:nil];
       }
    }
}
//Dismiss keyboard
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}


-(BOOL) checkIsDoubleValue:(NSString*) string
{
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES '^[-+]?[0-9]*\.?[0-9]+$'"];
    BOOL result = [predicate evaluateWithObject:string];
    return result;
}

@end
