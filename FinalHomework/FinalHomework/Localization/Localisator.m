//
//  Localisator.m
//  FinalHomework
//
//  Created by Vũ hoàng on 7/5/18.
//  Copyright © 2018 Vũ hoàng. All rights reserved.
//

#import "Localisator.h"


@interface Localisator()
    
    @property NSDictionary * dicoLocalisation;
    @property NSUserDefaults * defaults;
    
    @end

@implementation Localisator
#pragma  mark - Singleton Method
    
+ (Localisator*)sharedInstance
    {
        static Localisator *_sharedInstance = nil;
        
        static dispatch_once_t oncePredicate;
        
        dispatch_once(&oncePredicate, ^{
            _sharedInstance = [[Localisator alloc] init];
        });
        return _sharedInstance;
    }
    
    
#pragma mark - Init methods
    
- (id)init
    {
        self = [super init];
        if (self)
        {
             _availableLanguagesArray        = @[@"en", @"id"];
            NSString * languageDevice       = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
            
            if (![self.availableLanguagesArray containsObject:languageDevice]) {
                    [self loadDictionaryForLanguage:@"en"];
                }
            else
            {
                 [self loadDictionaryForLanguage:languageDevice];
            }

        }
        return self;
    }
    
    

-(BOOL)loadDictionaryForLanguage:(NSString *)codeLanguage
    {
        BOOL languageFound = NO;
        
        NSURL * urlPath = [[NSBundle bundleForClass:[self class]] URLForResource:@"Localizable" withExtension:@"strings" subdirectory:nil localization:codeLanguage];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:urlPath.path])
        {
            self.currentLanguage = [codeLanguage copy];
            self.dicoLocalisation = [[NSDictionary dictionaryWithContentsOfFile:urlPath.path] copy];
            
            languageFound = YES;
        }
        
        return languageFound;
    }
    
    
#pragma mark - Public Instance methods
    
-(NSString *)localizedStringForKey:(NSString*)key
    {
        if (self.dicoLocalisation == nil)
        {
            return NSLocalizedString(key, key);
        }
        else
        {
            NSString * localizedString = self.dicoLocalisation[key];
            if (localizedString == nil)
            localizedString = key;
            return localizedString;
        }
    }
    
-(BOOL)setLanguage:(NSString *)newLanguage
    {
        if (newLanguage == nil || [newLanguage isEqualToString:self.currentLanguage])
            return NO;
        if(![self.availableLanguagesArray containsObject:newLanguage])
            newLanguage = @"en";
        self.currentLanguage = [newLanguage copy];
        BOOL isLoadingOk = [self loadDictionaryForLanguage:newLanguage];
        return isLoadingOk;
    }
    

    @end

