//
//  Localisator.h
//  FinalHomework
//
//  Created by Vũ hoàng on 7/5/18.
//  Copyright © 2018 Vũ hoàng. All rights reserved.
//

#import <Foundation/Foundation.h>
#define LOCALIZATION(text) [[Localisator sharedInstance] localizedStringForKey:(text)]
@interface Localisator : NSObject
    @property (nonatomic, readonly) NSArray* availableLanguagesArray;
    @property NSString * currentLanguage;
    
+ (Localisator*)sharedInstance;
-(NSString *)localizedStringForKey:(NSString*)key;
-(BOOL)setLanguage:(NSString*)newLanguage;
    @end

