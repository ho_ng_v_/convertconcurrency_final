//
//  AppDelegate.h
//  FinalHomework
//
//  Created by Vũ hoàng on 7/5/18.
//  Copyright © 2018 Vũ hoàng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

