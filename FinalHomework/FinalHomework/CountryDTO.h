//
//  CountryDTO.h
//  FinalHomework
//
//  Created by Vũ hoàng on 7/6/18.
//  Copyright © 2018 Vũ hoàng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryDTO : NSObject
@property (nonatomic, strong) NSString *countryName;
@property (nonatomic, strong) NSString *countryCurrency;
@property (nonatomic, strong) NSString *countryCurrencySymbol;
@property (nonatomic, strong) NSString *languageId;
@end
