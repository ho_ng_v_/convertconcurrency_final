//
//  ViewController.h
//  FinalHomework
//
//  Created by Vũ hoàng on 7/5/18.
//  Copyright © 2018 Vũ hoàng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryDTO.h"
@interface ViewController : UIViewController
    @property (weak, nonatomic) IBOutlet UILabel *lblHello;
    @property (nonatomic,retain) NSMutableArray* listCountry;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerFirstCountry;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerSecondCountry;
@property (weak, nonatomic) IBOutlet UITextField *txtInputMoney;
@property (weak, nonatomic) IBOutlet UILabel *txtConvertedMoney;
@property (strong, atomic) CountryDTO* firstCountry;
@property (strong, atomic) CountryDTO* secondCountry;

@end

